// import React from 'react';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import Icon from 'react-native-vector-icons/MaterialIcons';

// import Movimentos from '../pages/Movimentos';
// import NovoMovimento from '../pages/Movimentos/Novo';
// // import MainRoutes from '../pages/Main';

// const Tab = createBottomTabNavigator();

// export default function DashboardRoutes() {
//   return (
//     <Tab.Navigator
//       screenOptions={{
//         headerShown: true,
//       }}
//       tabBarOptions={{
//         keyboardHidesTabBar: true,
//         activeTintColor: 'white',
//         inactiveTintColor: '#000',
//         headerShown: true,
//         style: {
//           backgroundColor: '#0b66b1',
//         },
//       }}
//     >
//       <Tab.Screen
//         name="Buscar"
//         component={Movimentos}
//         options={{
//           tabBarIcon: ({ color, size }) => (
//             <Icon name="search" color={color} size={size} />
//           ),
//         }}
//       />
//       <Tab.Screen
//         name="NovoMovimento"
//         component={NovoMovimento}
//         options={{
//           tabBarIcon: ({ color, size }) => (
//             <Icon name="add-circle-outline" color={color} size={size} />
//           ),
//         }}
//       />
//     </Tab.Navigator>
//   );
// }
