import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer } from 'redux-persist';

export default (reducers) => {
  const persistedReducer = persistReducer(
    {
      key: 'angelmobile',
      storage: AsyncStorage,
      whitelist: ['auth', 'usuario'],
    },
    reducers
  );
  return persistedReducer;
};
