import { combineReducers } from 'redux';

import auth from './auth/reducer';
import usuario from './user/reducer';

export default combineReducers({
  auth,
  usuario,
});
