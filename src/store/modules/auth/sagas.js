import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';
import { api } from '~/services/api';
// import history from '~/services/history';

import { signInSuccess, signFailure } from './actions';

export function* signIn({ payload }) {
  try {
    const { username, senha } = payload;

    const response = yield call(api.post, 'sessions', {
      username,
      senha,
    });

    const { token, user } = response.data;

    api.defaults.headers.Authorization = `Bearer ${token}`;

    yield put(signInSuccess(token, user));
  } catch (err) {
    Alert.alert(
      'Erro no login',
      'Houve uma falha no login, confira seus dados'
    );
    yield put(signFailure());
  }
}

export function* signUp({ payload }) {
  try {
    const { nome, username, senha, email } = payload;
    yield call(api.post, 'usuario', {
      nome,
      username,
      senha,
      email,
    });
    // history.push('/');
  } catch (err) {
    Alert.alert('Falha no cadastro, verifique seus dados');
    yield put(signFailure());
  }
}

export function setToken({ payload }) {
  if (!payload) return;
  const { token } = payload.auth;
  if (token) {
    api.defaults.headers.Authorization = `Bearer ${token}`;
  }
}

export function signOut() {
  // history.push('/');
}
export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('@auth/SIGN_UP_REQUEST', signUp),
  takeLatest('@auth/SIGN_OUT', signOut),
]);
