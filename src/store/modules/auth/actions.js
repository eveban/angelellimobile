export function signInRequest(username, senha) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { username, senha },
  };
}

export function signInSuccess(token, user) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: { token, user },
  };
}

export function signUpRequest(nome, username, email, senha) {
  return {
    type: '@auth/SIGN_UP_REQUEST',
    payload: { nome, username, email, senha },
  };
}

export function signFailure() {
  return {
    type: '@auth/SIGN_FAILURE',
  };
}

export function signOut() {
  return {
    type: '@auth/SIGN_OUT',
  };
}
