import { enableES5 } from 'immer';
import { createStore, compose, applyMiddleware } from 'redux';

enableES5();

export default (reducers, middlewares) => {
  const enhancer = __DEV__
    ? compose(console.tron.createEnhancer(), applyMiddleware(...middlewares))
    : applyMiddleware(...middlewares);
  return createStore(reducers, enhancer);
};
