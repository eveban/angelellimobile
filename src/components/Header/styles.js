import styled from 'styled-components/native';

export const HeaderContainer = styled.View`
  height: 60px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background: #c6222e;
  padding: 0 20px;
`;

export const ToggleButton = styled.TouchableOpacity`
  /* margin: 20px 30px; */
`;

export const HeaderTitle = styled.Text`
  color: #fff;
  font-family: 'Ubuntu-Medium';
  font-size: 19px;
`;
export const HeaderTitleInfo = styled.Text`
  color: #fff;
  font-family: 'Ubuntu-Medium';
  font-size: 22px;
  align-self: center;
`;
export const HeaderTitleInfoQtde = styled.Text`
  color: #fff;
  font-family: 'Ubuntu-Medium';
  font-size: 10px;
  align-self: flex-end;
`;
