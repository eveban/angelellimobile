import React from 'react';

import Icon from 'react-native-vector-icons/FontAwesome5';

import {
  HeaderContainer,
  ToggleButton,
  HeaderTitle,
  HeaderTitleInfo,
  HeaderTitleInfoQtde,
} from './styles';

const Header = ({ children, name, icon, navigation, color, size, title }) => {
  return (
    <HeaderContainer>
      <ToggleButton onPress={navigation}>
        <Icon name={icon} size={size} color={color} />
      </ToggleButton>
      <HeaderTitle>{name}</HeaderTitle>
      <ToggleButton>
        {children >= 0 ? (
          <>
            <HeaderTitleInfo>{children}</HeaderTitleInfo>
            <HeaderTitleInfoQtde>{title}</HeaderTitleInfoQtde>
          </>
        ) : (
          <Icon name="search" size={24} color="#c6222e" />
        )}
      </ToggleButton>
    </HeaderContainer>
  );
};

export default Header;
