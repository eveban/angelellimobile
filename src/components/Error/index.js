import React from 'react';
import { View, Text } from 'react-native';

const Error = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text style={{ fontSize: 18, textAlign: 'center' }}>
        Erro na conexão, verifique sua internet
      </Text>
    </View>
  );
};

export default Error;
