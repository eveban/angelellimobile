import React from 'react';

import { Container, Left, Info, Name } from './styles';

export default function Pedidos({ data }) {
  return (
    <Container>
      <Left>
        <Info>
          <Name> IDMOV: {data.idmov}</Name>
          <Name> Cód. Cliente: {data.codcfo}</Name>
        </Info>
      </Left>
    </Container>
  );
}
