import styled from 'styled-components/native';

export const Container = styled.View`
  padding: 0 15px;
  height: 64px;

  border-bottom-width: 1px;
  border-bottom-color: #b5b5b5;
  border-radius: 10px;

  flex-direction: row;
  align-items: center;

  /* background: #fff; */
`;

export const TInput = styled.TextInput.attrs({
  placeholderTextColor: '#b5b5b5',
})`
  flex: 1;
  font-size: 15px;
  margin: 0;
  margin-left: 10px;
  /* color: #fff; */
`;
