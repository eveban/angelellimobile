import { Platform, Image } from 'react-native';
import styled from 'styled-components/native';

import Input from '~/components/Input';
import Button from '~/components/Button';

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 30px;
`;

export const Form = styled.View`
  align-self: stretch;
  margin-top: 20px;
`;

export const TInput = styled(Input).attrs({
  placeholderTextColor: 'rgba(255, 255, 255, 0.8)',
})`
  margin-bottom: 10px;
`;

export const SubmitButton = styled(Button)`
  margin-top: 5px;
`;

export const SignLink = styled.TouchableOpacity`
  margin-top: 20px;
`;

export const SignLinkText = styled.Text`
  color: #fff;
  font-weight: bold;
  font-size: 16px;
`;
export const Logo = styled(Image)`
  width: 100px;
  height: 80px;
`;

export const Nome = styled.Text`
  font-size: 40px;
  color: #fff;
  font-weight: bold;
  font-style: italic;
`;
