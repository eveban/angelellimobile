import React, { useRef, useState } from 'react';
import { useDispatch } from 'react-redux';

// import Background from '~/components/Background';
import { signUpRequest } from '~/store/modules/auth/actions';

import {
  Container,
  Form,
  SubmitButton,
  SignLink,
  SignLinkText,
  TInput,
} from './styles';

export default function SignUp({ navigation }) {
  const dispatch = useDispatch();
  const senhaRef = useRef();
  const emailRef = useRef();
  const userNameRef = useRef();

  const [nome, setNome] = useState('');
  const [username, setUsername] = useState('');
  const [senha, setSenha] = useState('');
  const [email, setEmail] = useState('');

  function handleSubmit() {
    dispatch(signUpRequest(nome, username, email, senha));
  }

  return (
    <Container>
      <Form>
        <TInput
          icon="person-outline"
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Digite seu nome"
          returnKeyType="next"
          onSubmitEditing={() => userNameRef.current.focus()}
          value={nome}
          onChangeText={setNome}
        />
        <TInput
          icon="person-outline"
          autoCorrect={false}
          autoCapitalize="none"
          ref={userNameRef}
          placeholder="Usuário"
          returnKeyType="next"
          onSubmitEditing={() => emailRef.current.focus()}
          value={username}
          onChangeText={setUsername}
        />
        <TInput
          icon="mail-outline"
          autoCorrect={false}
          autoCapitalize="none"
          ref={emailRef}
          placeholder="E-mail"
          returnKeyType="next"
          onSubmitEditing={() => senhaRef.current.focus()}
          value={email}
          onChangeText={setEmail}
        />

        <TInput
          icon="lock-outline"
          secureTextEntry
          placeholder="Senha"
          ref={senhaRef}
          returnKeyType="send"
          onSubmitEditing={handleSubmit}
          value={senha}
          onChangeText={setSenha}
        />

        <SubmitButton onPress={handleSubmit}>Acessar</SubmitButton>
      </Form>

      <SignLink onPress={() => navigation.navigate('SignIn')}>
        <SignLinkText>Ja tenho Login</SignLinkText>
      </SignLink>
    </Container>
  );
}
