import styled from 'styled-components/native';
import TextInputMask from 'react-native-text-input-mask';

export const Container = styled.View`
  flex: 1;
  background-color: #ffffff;
`;

export const Form = styled.View`
  flex-direction: column;
  /* padding-bottom: 20px; */
  border-color: #056571;
  padding: 20px;
`;

export const Busca = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  padding: 0 10px;
  padding-bottom: 10px;

  background: #c6222e;
`;

export const InputMask = styled(TextInputMask)`
  width: 80%;
  height: 54px;
  background: #fff;
  border-radius: 10px;
  border: 1px solid #eee;
  text-align: center;
  font-size: 20px;
  font-family: 'Ubuntu-Medium';
  /* font-size: 18px; */
  color: #cc222e;
`;

export const SearchButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  background: #fff;
  border-radius: 10px;
  /* margin-left: 10px; */
  width: 55px;
  height: 55px;
`;

export const ContainerText = styled.View`
  height: 20px;
  padding: 0 10px;
  border-bottom-color: #ccc;
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`;
export const NomeCliente = styled.Text`
  color: #cc222e;
  font-family: 'Ubuntu-Bold';
  font-size: 18px;
  text-align: center;

  margin-bottom: 20px;
`;
export const TextCliente = styled.Text`
  color: #cc222e;
  font-family: 'Ubuntu-Medium';
  font-size: 16px;
`;

export const ContainerPagamento = styled.View`
  height: 20px;
  padding: 5px 10px;
  border-bottom-color: #ccc;
  margin-top: 10px;
  justify-content: space-between;
  align-items: center;
`;
export const TextPagamento = styled.Text`
  color: #cc222e;
  font-family: 'Ubuntu-Medium';
  font-size: 16px;
  padding: 0px 4px;
  margin-bottom: 8px;
`;
