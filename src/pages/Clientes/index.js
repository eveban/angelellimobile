import React, { useState } from 'react';
import { Keyboard, View } from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { api } from '../../services/api';
import Header from '../../components/Header';
import {
  Container,
  Form,
  InputMask,
  Busca,
  SearchButton,
  ContainerText,
  TextCliente,
  NomeCliente,
  TextPagamento,
  ContainerPagamento,
} from './styles';

export default function Clientes({ navigation }) {
  const [cliente, setCliente] = useState('');
  const [dadosCliente, setDadosCliente] = useState({});

  const handleSearchCliente = async () => {
    const response = await api.get(`cliente?codcfo=${cliente}`);
    setDadosCliente(response.data);
    setCliente('');
    Keyboard.dismiss();
  };

  return (
    <Container>
      <Header
        name="Buscar clientes"
        icon="arrow-circle-left"
        color="#fff"
        size={26}
        navigation={navigation.goBack}
      />
      <Busca>
        <InputMask
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Buscar cliente"
          returnKeyType="send"
          mask="[00].[0000]"
          onChangeText={(text) => setCliente(text)}
          value={cliente}
          keyboardType="phone-pad"
        />

        <SearchButton onPress={handleSearchCliente}>
          <FontAwesome name="search" size={20} color="#c6222e" />
        </SearchButton>
      </Busca>
      <Form>
        {dadosCliente.cliente?.nome ? (
          <View>
            <NomeCliente>{dadosCliente.cliente.nome}</NomeCliente>
            <ContainerText>
              <TextCliente>Rua: </TextCliente>
              <TextCliente>
                {dadosCliente.cliente.rua}, {dadosCliente.cliente.numero}
              </TextCliente>
            </ContainerText>
            <ContainerText>
              <TextCliente>Bairro: </TextCliente>
              <TextCliente>{dadosCliente.cliente.bairro}</TextCliente>
            </ContainerText>
            <ContainerText>
              <TextCliente>Cidade: </TextCliente>
              <TextCliente>{dadosCliente.cliente.cidade}</TextCliente>
            </ContainerText>
            <ContainerText>
              <TextCliente>CNPJ:</TextCliente>
              <TextCliente>{dadosCliente.cliente.cgccfo}</TextCliente>
            </ContainerText>
            <ContainerText>
              <TextCliente>Status:</TextCliente>
              <TextCliente>
                {dadosCliente.cliente.ativo === 1 ? 'Ativo' : 'Inativo'}
              </TextCliente>
            </ContainerText>
            <ContainerText>
              <TextCliente>Bloqueado:</TextCliente>
              <TextCliente>
                {dadosCliente.cliente.bloqueado === 1 ? 'Sim' : 'Não'}
              </TextCliente>
            </ContainerText>
            <ContainerText>
              <TextCliente>Aberto financeiro:</TextCliente>
              <TextCliente>
                R$ {String(dadosCliente.saldo).replace('.', ',')}
              </TextCliente>
            </ContainerText>
            <ContainerPagamento>
              <TextPagamento>Cond. Pagamento:</TextPagamento>
              <TextPagamento>
                {dadosCliente.condicaoPagamento[0].descricao}
              </TextPagamento>
            </ContainerPagamento>
          </View>
        ) : (
          <View
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          >
            <TextCliente>Cliente não encontrado</TextCliente>
          </View>
        )}
      </Form>
    </Container>
  );
}
