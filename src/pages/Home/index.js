/* eslint-disable no-console */
import React, { useCallback, useEffect } from 'react';
import { TouchableOpacity, Image, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import NetInfo from '@react-native-community/netinfo';

import Materialicons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { signOut } from '~/store/modules/auth/actions';
import { api } from '../../services/api';

// import AuthContext from '../../contexts/auth';

import vender from '../../assets/vender.png';
import contract from '../../assets/tabela.png';
import cliente from '../../assets/cliente.png';
import finance from '../../assets/finance.png';

import {
  Container,
  HeaderText,
  HeaderImage,
  HeaderContainerText,
  HeaderContainer,
  HeaderTextWelcome,
  HeaderImageContainer,
  CategoriesContainerText,
  CategoriesText,
  CategoriesContainer,
  CategoryButtonFirst,
  ImageButton,
  CategoryButtonSecond,
  TextLegend,
  TextLegendDescription,
  CategoriesContainerFirstColum,
  CategoriesContainerSecondColum,
  NavigationContainer,
} from './styles';

const Home = () => {
  const usuario = useSelector((state) => state.usuario.profile);
  const dispatch = useDispatch();

  const navigation = useNavigation();

  const handleLogout = useCallback(() => {
    dispatch(signOut());
  }, [dispatch]);

  useEffect(() => {
    NetInfo.fetch().then((state) => {
      if (!state.isConnected) {
        Alert.alert(
          'Atenção',
          'Sem conexão com internet, cuidado ao lançar seus pedidos'
        );
      }
      // console.log('Type connection: ', state.type);
      // console.log('Is connected?', state.isConnected);
    });

    navigation.addListener('focus', async () => {
      await api.get('/');
    });
  }, [navigation]);

  return (
    <Container>
      <HeaderContainer>
        <HeaderContainerText>
          <HeaderTextWelcome>Olá, {usuario?.nome}</HeaderTextWelcome>
          <HeaderText>
            A forma mais simples de gerenciar seus pedidos.
          </HeaderText>
        </HeaderContainerText>
        <HeaderImageContainer>
          <HeaderImage
            source={{
              uri: `https://server.angelelli.com.br/uploads/${usuario?.username.toLowerCase()}.png`,
            }}
          />
        </HeaderImageContainer>
      </HeaderContainer>

      <CategoriesContainerText>
        <CategoriesText>Categorias</CategoriesText>
      </CategoriesContainerText>
      <ScrollView>
        <CategoriesContainer>
          <CategoriesContainerFirstColum>
            <CategoryButtonFirst
              onPress={() => navigation.navigate('Movimentos')}
            >
              <ImageButton>
                <Image style={{ width: 128, height: 128 }} source={vender} />
              </ImageButton>
              <TextLegend>Movimentos</TextLegend>
              <TextLegendDescription>
                Hora de lançar seus pedidos no sistema.
              </TextLegendDescription>
            </CategoryButtonFirst>
            <CategoryButtonSecond
              onPress={() => navigation.navigate('Cliente')}
            >
              <ImageButton>
                <Image style={{ width: 98, height: 80 }} source={cliente} />
              </ImageButton>
              <TextLegend>Clientes</TextLegend>
              <TextLegendDescription>
                Seus clientes na palmão da sua mão.
              </TextLegendDescription>
            </CategoryButtonSecond>
          </CategoriesContainerFirstColum>
          <CategoriesContainerSecondColum>
            <CategoryButtonSecond onPress={() => navigation.navigate('Tabela')}>
              <ImageButton>
                <Image style={{ width: 96, height: 96 }} source={contract} />
              </ImageButton>
              <TextLegend>Tabela de preços</TextLegend>
              <TextLegendDescription>
                Tabela de preço dos produtos.
              </TextLegendDescription>
            </CategoryButtonSecond>
            <CategoryButtonFirst
              onPress={() => navigation.navigate('Financeiro')}
            >
              <ImageButton>
                <Image
                  style={{ width: 128, height: 128, marginTop: 100 }}
                  source={finance}
                />
              </ImageButton>
              <TextLegend>Contas de receber</TextLegend>
              <TextLegendDescription>
                Listagem de títulos atrasados
              </TextLegendDescription>
            </CategoryButtonFirst>
          </CategoriesContainerSecondColum>
        </CategoriesContainer>
      </ScrollView>
      <NavigationContainer>
        <TouchableOpacity onPress={() => navigation.navigate('Movimentos')}>
          <Ionicons name="cart" size={26} color="#5799d5" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Financeiro')}>
          <MaterialCommunityIcons name="finance" size={26} color="#399d4b" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Tabela')}>
          <Materialicons name="list-alt" size={26} color="#ebbd5f" />
        </TouchableOpacity>
        <TouchableOpacity onPress={handleLogout}>
          <Materialicons name="logout" size={26} color="#c6222e" />
        </TouchableOpacity>
      </NavigationContainer>
    </Container>
  );
};

export default Home;
