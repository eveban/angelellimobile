import styled from 'styled-components/native';
import { Image } from 'react-native';

export const Container = styled.View`
  flex: 1;
  background-color: #f7f7f7;
`;

export const HeaderText = styled.Text`
  font-size: 14px;
  color: #ffffff;
`;

export const HeaderContainerText = styled.View`
  flex: 1;
  flex-direction: column;
`;

export const HeaderContainer = styled.View`
  width: 100%;
  height: 15%;
  padding: 30px;
  background-color: #c6222e;

  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  elevation: 10;

  /* border-bottom-right-radius: 40px; */
`;

export const HeaderTextWelcome = styled.Text`
  font-family: 'Poppins-SemiBold';
  font-size: 18px;
  color: #ffffff;
`;

export const HeaderContainerTextImage = styled.View`
  width: 100%;
  padding: 0 30px;
  background-color: #c6222e;
  border-bottom-left-radius: 100px;
`;

export const HeaderImageContainer = styled.View`
  width: 68px;
  height: 68px;
  border-radius: 34px;
  justify-content: center;
  align-items: center;
  border: 2px #fff;

  background-color: #c6222e;

  elevation: 5;
`;

export const HeaderImage = styled(Image)`
  width: 62px;
  height: 62px;
  border-radius: 32px;
`;

export const ContainerScrollView = styled.View`
  flex: 1;
  border: 1px solid #fff;
`;

export const CategoriesContainerText = styled.View`
  width: auto;
  padding-left: 20px;
`;

export const CategoriesText = styled.Text`
  font-size: 18px;
  font-family: 'Poppins-SemiBold';
  color: #564242;

  margin-top: 10px;
`;

export const CategoriesContainer = styled.View`
  flex: 1;
  margin-top: 10px;
  padding: 0 20px;

  flex-direction: row;
`;
export const CategoriesContainerFirstColum = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
  margin-right: 5px;
`;
export const CategoriesContainerSecondColum = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
  margin-left: 5px;

  elevation: 5;
`;

export const CategoryButtonFirst = styled.TouchableOpacity`
  flex: 1;
  width: 100%;
  height: 60%;
  background-color: #fff;
  border-radius: 10px;
  margin-bottom: 10px;

  elevation: 5;
`;

export const ImageButton = styled.View`
  height: 55%;
  /* background-color: rgba(57, 157, 75, 0.3); */
  background-color: #fff;

  align-items: center;
  justify-content: flex-end;

  margin: 0 10px;

  border-bottom-width: 1px;
  border-bottom-color: rgba(86, 66, 66, 0.4);

  /* border-top-left-radius: 10px; */
  border-top-right-radius: 10px;
`;

export const CategoryButtonSecond = styled.TouchableOpacity`
  width: 100%;
  height: 38%;
  background-color: #fff;
  border-radius: 10px;
  margin-bottom: 10px;

  elevation: 5;
`;

export const TextLegend = styled.Text`
  margin-left: 10px;
  margin-top: 10px;
  font-size: 14px;
  font-family: 'Poppins-SemiBold';
  color: rgb(86, 66, 66);
`;
export const TextLegendDescription = styled.Text`
  margin-left: 10px;
  font-size: 12px;
  font-family: 'Roboto_700Bold';
  color: rgba(86, 66, 66, 0.6);
`;

export const NavigationContainer = styled.View`
  width: 90%;
  height: 68px;
  border-radius: 16px;
  background: #fff;

  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  align-self: center;

  margin-bottom: 30px;

  elevation: 5;
`;
