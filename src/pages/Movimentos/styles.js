import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { TouchableOpacity, Platform } from 'react-native';

import RNPickerSelect from 'react-native-picker-select';
import TextInputMask from 'react-native-text-input-mask';

export const Container = styled.SafeAreaView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
  background: #f5f5f5;
`;

export const InputContainer = styled.View`
  padding: 0 15px;
  flex-direction: row;
  justify-content: space-between;
  background: #c6222e;
  height: 40px;
`;

export const HeaderContainer = styled.View`
  flex-direction: column;
  background: #c6222e;
`;

export const Input = styled(TextInputMask)`
  width: 65%;
  height: 50px;
  background: #fff;
  border-radius: 8px;
  padding: 0 15px;
`;
export const InputPicker = styled(RNPickerSelect)`
  width: 65%;
  height: 50px;
  background: #fff;
  border-radius: 8px;
  padding: 0 15px;
`;

export const Form = styled.View`
  flex-direction: row;
  padding-bottom: 20px;
  margin: 0 5px 0;
`;

export const SubmitButton = styled(RectButton).attrs({
  placeholderTextColor: '#999',
})`
  justify-content: center;
  align-items: center;
  background: #fff;
  border-radius: 8px;
  margin-left: 10px;
  width: 50px;
  height: 50px;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})`
  margin-top: 10px;
`;

export const PedidoButton = styled.TouchableOpacity`
  height: 80px;
  border-radius: 8px;
  align-items: center;
  margin: 5px 10px;
  background: #ffffff;
  border: 1px solid ${(props) => (props.romaneio ? `#01cc43` : '#FFFFFF')};
  elevation: 3;
  padding: 4px;
`;

export const PedidoEdit = styled(TouchableOpacity)`
  flex: 1;
  width: 80px;
  height: 80px;
  right: 0;
  border-radius: 10px;
  margin: 5px 10px;
  background: #00cc00;
  align-items: center;
  justify-content: center;
  position: absolute;
`;
export const PedidoDelete = styled(TouchableOpacity)`
  flex: 1;
  width: 80px;
  height: 80px;
  left: 0;
  border-radius: 10px;
  margin: 5px 10px;
  background: #c6222e;
  align-items: center;
  justify-content: center;
  position: absolute;
`;

export const Info = styled.View`
  height: 80px;
  border-radius: 8px;
  align-items: center;
  margin: 5px 10px;
  background-color: #fff;
  elevation: 3;
  padding: 4px;
`;

export const Codigo = styled.Text`
  font-size: 14px;
  color: #c6222e;
  font-weight: bold;
  margin-top: 4px;
`;

export const DatePedido = styled.Text`
  font-size: 16px;
  color: #0087cc;
  margin-top: 4px;
  font-family: 'Ubuntu-Bold';
  text-align: center;
`;
export const ContainerLista = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 10px;
`;
export const Name = styled.Text`
  font-size: 16px;
  color: ${(props) => (props.romaneio ? '#00cc43' : '#c6222e')};
  margin-bottom: 4px;
  font-family: 'Ubuntu-Bold';
`;

export const ProfileButton = styled(RectButton)`
  margin-top: 5px;
  align-self: stretch;
  border-radius: 4px;
  background: #c6222e;
  justify-content: center;
  align-items: center;
  height: 36px;
`;

export const ProfileButtonText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: #fff;
  text-transform: uppercase;
`;

export const ActionContainer = styled.View`
  /* flex: 1; */
  /* justify-content: space-between; */
  /* align-items: flex-end; */
  /* flex-direction: row; */

  /* background: transparent; */

  /* padding: 30px 10px; */
`;

export const AddAction = styled.TouchableOpacity`
  /* border: 1px solid rgba(255, 255, 255, 0.9); */
  width: 70px;
  height: 70px;
  border-radius: 35px;
  background-color: #399d4b;
  justify-content: center;
  align-items: center;
  /* margin: 20px; */
  elevation: 7;
`;
export const AddActionTruck = styled.TouchableOpacity`
  /* border: 1px solid rgba(255, 255, 255, 0.9); */
  width: 70px;
  height: 70px;
  border-radius: 35px;
  background-color: #0087cc;
  justify-content: center;
  align-items: center;
  /* margin: 20px; */
  elevation: 7;
`;
export const ButtonBalance = styled.TouchableOpacity`
  /* border: 1px solid rgba(255, 255, 255, 0.9); */
  width: 70px;
  height: 70px;
  border-radius: 35px;
  background-color: #9b59b6;
  justify-content: center;
  align-items: center;
  /* margin: 20px; */
  elevation: 7;
`;
export const ButtonReport = styled.TouchableOpacity`
  /* border: 1px solid rgba(255, 255, 255, 0.9); */
  width: 70px;
  height: 70px;
  border-radius: 35px;
  background-color: #cb4459;
  justify-content: center;
  align-items: center;
  /* margin: 20px; */
  elevation: 7;
`;
export const PickerContainer = styled.View`
  width: 95%;
  height: 60px;

  background: #f5f5f5;
  border-radius: 10px;
  border: 1px solid #e5e5e5;
  margin: 20px 10px;
  font-size: 5px;
  align-items: center;
  justify-content: center;
  /* padding: 10px 20px; */
`;
export const PickerContainerTipoMovimento = styled.View`
  height: 50px;

  background: #f5f5f5;
  border-radius: 10px;
  border: 1px solid #e5e5e5;

  margin-top: 20px;
  margin-bottom: 20px;
  margin-left: 15px;
  margin-right: 10px;

  align-items: center;
  justify-content: center;
  padding: 10px 20px;
`;
export const PickerTPMovimentoContainer = styled.View`
  height: 50px;

  background: #c6222e;
  font-size: 5px;
  align-items: center;
  /* justify-content: center; */
`;

export const ButtonContainerModalRomaneio = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;
export const ButtonModal = styled.TouchableOpacity`
  height: 60px;
  width: 45%;
  /* background: #399d4b; */
  border-radius: 10px;
  align-items: center;
  justify-content: center;

  /* margin-bottom: 20px; */

  /* align-self: flex-end; */
`;
export const ButtonModalPeso = styled.TouchableOpacity`
  height: 60px;
  width: 80%;
  background: #399d4b;
  border-radius: 10px;
  align-items: center;
  justify-content: center;
  margin-bottom: 20px;

  align-self: flex-end;
`;
export const ModalPeso = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const TextContainer = styled.View`
  flex-direction: column;
  margin-top: 10px;
`;
export const TextPesoHeader = styled.Text`
  margin-top: 5px;
  font-size: 16px;
  color: #399d4b;
  align-self: center;
  font-family: 'Ubuntu-Bold';
`;
export const TextPeso = styled.Text`
  font-size: 16px;
  color: #399d4b;

  font-family: 'Ubuntu-Bold';
`;
