import React, { useEffect, useState } from 'react';
import { Keyboard } from 'react-native';
import { useRoute } from '@react-navigation/native';

import moment from 'moment';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { api } from '~/services/api';
import HeaderComponent from '../../../components/Header';
import Loading from '../../../components/Loading';
import Error from '../../../components/Error';
import {
  Container,
  Header,
  HeaderInfo,
  TextCodigo,
  TextName,
  PedidoDataContainer,
  Text,
  ListContainer,
  ButtonList,
  TextProduto,
  TextPreco,
  ItemDelete,
  ButtonDelete,
  ContainerInfoList,
} from './styles';

const Detalhes = ({ navigation }) => {
  const route = useRoute();
  const { id } = route.params;
  const [codigoCliente, setCodigoCliente] = useState(0);
  // eslint-disable-next-line no-unused-vars
  const [idMov, setIdMov] = useState(0);
  const [numeroMovimento, setNumeroMovimento] = useState(0);
  const [nome, setNome] = useState('');
  const [dataEmissao, setDataEmissao] = useState('');
  const [itemsPedido, setItemsPedido] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [status, setStatus] = useState('');
  const [error, setError] = useState(null);

  const initialState = () => {
    setCodigoCliente('');
    setIdMov('');
    setNumeroMovimento('');
    setNome('');
    setDataEmissao('');
    setItemsPedido([]);
  };

  const handleDeleteItem = (identificador, nseqitmmov) => {
    api.delete(`movimento/item/${identificador}/${nseqitmmov}/delete`);
    const listItem = itemsPedido.filter(
      (item) => item.nseqitmmov !== nseqitmmov
    );
    setItemsPedido(listItem);
  };

  useEffect(() => {
    navigation.addListener('focus', async () => {
      const fetchData = async () => {
        setIsLoading(true);
        const pedido = await api.get(`movimento/${id}`);
        const { movimento, items } = pedido.data;
        setCodigoCliente(movimento.codcfo);
        setIdMov(movimento.id);
        setNumeroMovimento(movimento.numeroMovimento);
        setNome(movimento.cliente.nome);
        setDataEmissao(movimento.dataEmissao);
        setStatus(movimento.status);
        setItemsPedido(items);
        setIsLoading(false);
      };
      try {
        fetchData();
      } catch (err) {
        setTimeout(() => {
          setIsLoading(false);
          setError(err);
        }, 5000);
      }

      return () => {
        initialState();
      };
    });
  }, [id, navigation]);

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  const formatMoney = (value) => {
    const valorFormatado =
      String(value.toFixed(2))
        .replace('.', ',')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') || 0;
    return valorFormatado;
  };

  async function handleAddItem() {
    navigation.navigate('NovoItem', {
      idMov: id,
      dataEmissao: dataEmissao.substring(0, 10),
      dataEntrega: dataEmissao.substring(0, 10),
    });
    Keyboard.dismiss();
  }

  return (
    <Container>
      <HeaderComponent
        icon="arrow-left"
        color="#fff"
        size={24}
        navigation={() => navigation.navigate('Movimentos')}
        name="Detalhes"
      />
      <Header>
        <HeaderInfo>
          <TextCodigo>{codigoCliente}</TextCodigo>
          <TextName>{nome.substring(0, 30)}</TextName>
          <PedidoDataContainer>
            <Text>Pedido №: {numeroMovimento}</Text>
            <Text>
              {moment(dataEmissao, 'YYYY-MM-DD').format('DD/MM/YYYY')}
            </Text>
          </PedidoDataContainer>
        </HeaderInfo>
      </Header>
      <ListContainer
        key={(item) => item.nseqitmmov}
        data={itemsPedido}
        renderItem={({ item }) => (
          <ButtonList>
            <ContainerInfoList>
              <TextProduto>Código: {item.produto.codigoAuxiliar}</TextProduto>
              <TextProduto>{item.produto.fantasia}</TextProduto>
            </ContainerInfoList>

            <ContainerInfoList>
              <TextPreco>Quantidade: {item.quantidade}</TextPreco>
              <TextPreco>R$ {formatMoney(item.precoUnitario)}</TextPreco>
            </ContainerInfoList>
          </ButtonList>
        )}
        renderHiddenItem={({ item }) =>
          status === 'A' && (
            <ItemDelete
              onPress={() => {
                handleDeleteItem(item.id, item.nseqitmmov);
              }}
            >
              <FontAwesome name="trash" color="#fff" size={26} />
            </ItemDelete>
          )
        }
        // leftOpenValue={75}
        rightOpenValue={-85}
      />
      {status === 'A' && (
        <ButtonDelete onPress={() => handleAddItem()}>
          <Text>Adicionar Item</Text>
        </ButtonDelete>
      )}
    </Container>
  );
};

export default Detalhes;
