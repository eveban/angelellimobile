import styled from 'styled-components/native';
import { SwipeListView } from 'react-native-swipe-list-view';

export const Container = styled.View`
  flex: 1;
`;
export const Header = styled.View`
  width: 100%;
  height: 15%;
  border-bottom-right-radius: 50px;
  background: #c6222e;
  padding: 0 20px;
`;
export const HeaderInfo = styled.View`
  flex: 1;
  align-items: center;
`;

export const TextCodigo = styled.Text`
  color: #fff;
  font-size: 24px;
  font-family: 'Ubuntu-Bold';
  /* padding-top: 40px; */
`;
export const TextName = styled.Text`
  color: #fff;
  font-size: 18px;
  font-family: 'Ubuntu-Medium';
  padding-top: 10px;
`;
export const PedidoDataContainer = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export const Text = styled.Text`
  color: #fff;
  font-size: 18px;
  font-family: 'Ubuntu-Medium';
  padding-top: 5px;
`;
export const ButtonContainer = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  width: 100%;
`;
export const Button = styled.TouchableOpacity`
  border: 1px solid #fff;
  width: 48%;
  height: 50px;
  border-radius: 14px;
  align-items: center;
  justify-content: center;
`;

export const ListContainer = styled(SwipeListView)`
  flex: 1;
  padding: 0px 10px;
`;

export const ButtonList = styled.TouchableOpacity`
  border: 1px solid rgba(198, 34, 46, 0.2);
  width: 100%;
  height: 80px;
  border-radius: 10px;
  margin-top: 10px;
  padding: 5px 10px;
  background: #fff;
  /* background: rgba(0, 204, 73, 0.7); */
`;
export const ContainerInfoList = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const TextList = styled.Text`
  color: #c6222e;
  font-family: 'Ubuntu-Medium';
  font-size: 14px;
`;

export const TextProduto = styled.Text`
  color: #c6222e;
  font-family: 'Ubuntu-Bold';
  font-size: 16px;
  /* text-align: center; */
`;
export const TextPreco = styled.Text`
  color: #c6222e;
  font-family: 'Ubuntu-Bold';
  font-size: 18px;
  /* text-align: center; */
`;

export const ButtonDelete = styled.TouchableOpacity`
  width: 100%;
  height: 56px;
  background: #c6222e;
  /* align-self: flex-end; */
  align-items: center;
  justify-content: center;
`;
export const ItemDelete = styled.TouchableOpacity`
  flex: 1;
  width: 76px;
  height: 76px;
  right: 0;
  border-radius: 12px;
  margin: 11px 1px;
  background: #c6222e;
  align-items: center;
  justify-content: center;
  position: absolute;
`;
