import styled from 'styled-components/native';
import { TextInput, Platform, TouchableOpacity } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import { TextInputMask } from 'react-native-masked-text';

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
`;
export const HeaderInput = styled.View`
  height: 70px;
  background: #c6222e;
`;
export const InputContainer = styled.View`
  padding: 0px 20px;
  flex-direction: row;
  justify-content: space-between;
`;
export const SubmitButton = styled(RectButton).attrs({
  placeholderTextColor: '#999',
})`
  width: 50px;
  height: 50px;
  justify-content: center;
  align-items: center;
  background: #fff;
  border-radius: 8px;
`;
export const Input = styled(TextInput)`
  flex: 1;
  margin-right: 15px;
  width: 80%;
  background: #fff;
  border-radius: 8px;
  padding: 0 15px;
`;

export const Form = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
  padding: 10px 15px;
  align-items: center;
`;
export const TextContainer = styled.View`
  width: 100%;
  height: 70px;
  /* border: 1px solid #d91c5c; */
  border-radius: 8px;
  align-items: center;
  justify-content: center;
  background: #1485cc;
  elevation: 2;
  padding: 10px;
`;
export const Text = styled.Text`
  text-align: center;
  font-size: 16px;
  font-family: 'Ubuntu-Medium';
  color: #fff;
  margin: 2px;
`;
export const InputBody = styled(TextInput)`
  width: 100%;
  height: 60px;
  background: #fff;
  border-radius: 8px;
  padding: 15px;
  margin-top: 10px;
  border: 1px solid #e5e5e5;
  elevation: 2;
`;
export const InputObs = styled(TextInput)`
  width: 100%;
  height: 80px;
  background: #fff;
  border-radius: 8px;
  padding: 15px;
  margin-top: 10px;
  border: 1px solid #e5e5e5;
  elevation: 2;
`;

export const InputMask = styled(TextInputMask)`
  width: 100%;
  height: 60px;
  background: #fff;
  border-radius: 8px;
  padding: 15px;
  margin-top: 10px;
  border: 1px solid #e5e5e5;
  elevation: 2;
`;

export const AddButton = styled(TouchableOpacity)`
  width: 100%;
  height: 60px;
  justify-content: center;
  align-items: center;
  background: #00cc3a;
  border-radius: 8px;
  margin-top: 10px;
`;
export const FinishButton = styled(TouchableOpacity)`
  width: 100%;
  height: 60px;
  justify-content: center;
  align-items: center;
  background: #0087cc;
  border-radius: 8px;
  margin-top: 10px;
`;
