import React, { useState } from 'react';
import { useRoute } from '@react-navigation/native';
import { Alert, ScrollView, Platform } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Header from '~/components/Header';

import { api } from '~/services/api';

import {
  Container,
  HeaderInput,
  InputContainer,
  Input,
  InputObs,
  InputMask,
  Form,
  TextContainer,
  Text,
  InputBody,
  SubmitButton,
  AddButton,
  FinishButton,
} from './styles';

// console.disableYellowBox = true;

export default function Item({ navigation }) {
  const [produto, setProduto] = useState('');
  const [id, setId] = useState(0);
  const [fantasia, setFantasia] = useState('');
  const [codigoAuxiliar, setCodigoAuxiliar] = useState('');
  const [descricaoAuxiliar, setDescricaoCodigoAuxiliar] = useState('');
  const [preco, setPreco] = useState(0);
  const [quantidade, setQuantidade] = useState(0);
  const [unidadeControle, setUnidadeControle] = useState('');
  const [codtb2fat, setCodtb2fat] = useState('');
  const [codtb3fat, setCodtb3fat] = useState('');
  const [codtb4fat, setCodtb4fat] = useState('');
  const [observacao, setObservacao] = useState('');

  const route = useRoute();
  const routeParams = route.params;

  const handleAddItem = async () => {
    if (!preco || preco < 1) {
      Alert.alert('Atenção', `Preço ${fantasia} obrigatório`);
      return;
    }
    if (!quantidade || quantidade < 1) {
      Alert.alert('Atenção', `Quantidade ${fantasia} obrigatória.`);
      return;
    }

    await api.post('movimentos/adicionaItem/', {
      id,
      idMov: routeParams.idMov,
      preco,
      quantidade,
      unidadeControle,
      dataEmissao: routeParams.dataEmissao,
      dataEntrega: routeParams.dataEntrega,
      codtb2fat,
      codtb3fat,
      codtb4fat,
      observacao,
    });
    setProduto('');
    setId(0);
    setFantasia('');
    setCodigoAuxiliar('');
    setUnidadeControle('');
    setPreco(0);
    setQuantidade(0);
    setCodtb2fat('');
    setCodtb3fat('');
    setCodtb4fat('');
    setObservacao('');
  };

  const handleSearchItem = async () => {
    const response = await api.get(`produto?codigoAuxiliar=${produto}`);
    const { message } = response.data;
    if (message) {
      Alert.alert('Atenção', message);
    }
    const product = response.data;
    setFantasia(product.fantasia);
    setId(product.idprd);
    setCodigoAuxiliar(product.codigoAuxiliar);
    setDescricaoCodigoAuxiliar(product.descAuxiliar);
    setUnidadeControle(product.defaults.unidadeControle);
    setPreco(product.defaults.preco1);
    setCodtb2fat(product.defaults.codtb2fat);
    setCodtb3fat(product.defaults.codtb3fat);
    setCodtb4fat(product.defaults.codtb4fat);
  };

  return (
    <Container behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <Header icon="arrow-circle-left" color="#c6222e" name="Adicionar Itens" />
      <HeaderInput>
        <InputContainer>
          <Input
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Buscar produto"
            returnKeyType="send"
            onChangeText={(text) => setProduto(text)}
            value={produto}
            keyboardType="phone-pad"
          />

          <SubmitButton onPress={handleSearchItem}>
            <MaterialIcons name="search" size={20} color="#c6222e" />
          </SubmitButton>
        </InputContainer>
      </HeaderInput>
      <ScrollView keyboardShouldPersistTaps="always">
        <Form>
          {codigoAuxiliar ? (
            <TextContainer>
              <Text>
                {codigoAuxiliar} - {fantasia} - {unidadeControle}
              </Text>
              <Text>{descricaoAuxiliar}</Text>
            </TextContainer>
          ) : null}

          <InputMask
            type="money"
            placeholder="Valor do contrato"
            value={preco}
            onChangeText={(value) => {
              setPreco(value);
              value = value.replace('R$', '');
              value = value.replace('.', '');
              value = value.replace(',', '.');
              setPreco(Number(value));
            }}
            keyboardType="phone-pad"
          />
          <InputBody
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Quantidade"
            onChangeText={(text) => setQuantidade(text)}
            value={quantidade}
            keyboardType="phone-pad"
          />
          <InputObs
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Observações"
            onChangeText={(text) => setObservacao(text)}
            value={observacao}
            multiline
            textAlignVertical="top"
            keyboardType="visible-password"
          />
          <AddButton onPress={handleAddItem}>
            <Text>Adicionar</Text>
          </AddButton>
          <FinishButton onPress={() => navigation.navigate('Movimentos')}>
            <Text>Concluir Pedido</Text>
          </FinishButton>
        </Form>
      </ScrollView>
    </Container>
  );
}
