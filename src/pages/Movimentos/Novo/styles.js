import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import TextInputMask from 'react-native-text-input-mask';

export const Container = styled.View`
  flex: 1;
`;

export const Form = styled.View`
  flex-direction: column;
  /* padding-bottom: 20px; */
  border-color: #056571;
  padding: 20px;
`;
export const HeaderForm = styled.View`
  /* flex-direction: column; */
`;
export const HeaderMov = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const HeaderText = styled.Text`
  font-size: 14px;
  color: #c6222e;
  text-transform: uppercase;
`;
export const MaskSaldo = styled(TextInputMask)`
  border: 1px solid;
  flex: 1;
  font-weight: bold;
  color: #c6222e;
`;
export const Bold = styled.Text`
  font-weight: bold;
`;

export const Busca = styled.View`
  flex-direction: row;
`;

export const InputMask = styled(TextInputMask)`
  flex: 1;
  height: 60px;
  background: #eee;
  border-radius: 10px;
  padding: 0 15px;
  /* border: 1px solid rgba(255, 255, 255, 0.1); */
  border: 1px solid #ccc;
  margin-top: 10px;
  font-size: 18px;
  font-family: 'Ubuntu-Regular';
`;
export const InputText = styled(TextInputMask)`
  height: 60px;
  background: #eee;
  border-radius: 10px;
  padding: 0 15px;
  border: 1px solid #eee;
  margin-top: 10px;
  border: 1px solid #ccc;
`;

export const SearchButton = styled(RectButton)`
  justify-content: center;
  align-items: center;
  background: #c6222e;
  border-radius: 10px;
  margin-left: 10px;
  width: 60px;
  margin-top: 10px;
`;

export const Input = styled.TextInput`
  height: 60px;
  background: #eee;
  border-radius: 10px;
  padding: 0 15px;
  border: 1px solid #ccc;
  margin-top: 10px;
`;

export const SubmitButton = styled(RectButton)`
  justify-content: center;
  align-items: center;
  background: ${(props) => props.backgroundColor};
  border-radius: 10px;
  margin-top: 10px;
  height: 60px;
  opacity: 2;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})`
  margin-top: 20px;
`;

export const Dados = styled.View``;

export const Name = styled.Text`
  font-size: 14px;
  color: #333;
  font-weight: bold;
  margin-top: 4px;
  text-align: center;
`;

export const Codigo = styled.Text`
  font-size: 14px;
  color: #333;
  font-weight: bold;
  margin-top: 4px;
  text-align: left;
`;

export const ProfileButton = styled(RectButton)`
  margin-top: 5px;
  align-self: stretch;
  border-radius: 4px;
  background: #c6222e;
  justify-content: center;
  align-items: center;
  height: 36px;
`;

export const ProfileButtonText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: #fff;
  text-transform: uppercase;
`;

export const ActionContainer = styled.View`
  flex: 1;
  justify-content: flex-end;
  align-items: flex-end;
`;

export const AddAction = styled.TouchableOpacity`
  /* border: 1px solid rgba(255, 255, 255, 0.9); */
  width: 70px;
  height: 70px;
  border-radius: 35px;
  background-color: #c6222e;
  justify-content: center;
  align-items: center;
  elevation: 7;
`;

export const TextButton = styled.Text`
  color: #fff;
  font-family: 'Ubuntu-Medium';
  font-size: 18px;
`;

export const PickerContainer = styled.View`
  height: 60px;
  background: #eee;
  border-radius: 10px;
  border: 1px solid #ccc;
  margin-top: 10px;
  font-size: 5px;
  align-items: center;
  justify-content: center;
  padding: 0 15px;
`;

// padding: 0 15px;
// border: 1px solid #eee;

// border: 1px solid #ccc;
