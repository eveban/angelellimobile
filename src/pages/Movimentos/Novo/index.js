import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useRoute } from '@react-navigation/native';
import { Keyboard, Text, Alert } from 'react-native';

import moment from 'moment';
// import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { format, addDays } from 'date-fns';

import { api } from '~/services/api';
import Header from '~/components/Header';
import {
  Container,
  Form,
  HeaderForm,
  HeaderMov,
  HeaderText,
  InputMask,
  Busca,
  SearchButton,
  SubmitButton,
  Input,
  Bold,
  TextButton,
} from './styles';

export default function Movimentos({ navigation }) {
  const route = useRoute();
  const usuario = useSelector((state) => state.usuario.profile);

  // const auth = useSelector((state) => state.auth);

  const [cliente, setCliente] = useState('');
  const [dataEmissao, setDataEmissao] = useState(
    format(addDays(new Date(), 1), 'dd/MM/yyyy')
  );
  const [nome, setNome] = useState('');
  const [codigo, setCodigo] = useState('');
  const [cnpj, setCNPJ] = useState('');
  const [romaneio, setRomaneio] = useState('');
  const [id, setId] = useState(null);
  const [numeroMov, setNumeroMov] = useState(null);
  const [saldo, setSaldo] = useState(0);
  const [condicaoPagamento, setCondicaoPagamento] = useState(null);
  const [representante, setRepresentante] = useState(null);
  const routeParams = route.params;

  async function handleSaveMovimento() {
    setNome('');
    setCodigo('');
    setCNPJ('');
    setCliente('');
    setSaldo(0);
    setRomaneio('');
  }

  useEffect(() => {
    setId(routeParams.id);
    setNumeroMov(routeParams.numeroMov);
    handleSaveMovimento();
  }, [routeParams]);

  async function handleSearchCliente() {
    const response = await api.get(`movimentos/cliente?codcfo=${cliente}`);
    const { message } = response.data;
    if (message) {
      Alert.alert('Atenção', `${message}  \n\nCONSULTAR SETOR DE CADASTROS`);
      handleSaveMovimento();
      return;
    }

    const clienteData = response.data.cliente;
    setNome(clienteData.nome);
    setCodigo(clienteData.codcfo);
    setCNPJ(clienteData.cgccfo);
    // setLimite(clienteData.complementar.limite);
    setCondicaoPagamento(clienteData.condicaoPagamento.condicaoPagamento);
    setRepresentante(clienteData.defaults.representante);
    setSaldo(response.data.saldo);
    Keyboard.dismiss();
  }

  // async function handleAddMovimento() {
  //   // const response = await api.get(`movimentos/novoPedido`);
  //   // setIdMov(response.data);
  //   // console.tron.log(response.data);
  //   setNome('');
  //   setCodigo('');
  //   setCNPJ('');
  //   setLimite(0);
  //   setCliente('');
  // }

  async function handleAddItem() {
    const dataCriacao = format(new Date(), 'yyyy-MM-dd');

    await api.post(`movimentos`, {
      id,
      numeroMov,
      dataEmissao: moment(dataEmissao, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      dataCriacao,
      codcfo: codigo,
      condicaoPagamento,
      representante,
      dataSaida: moment(dataEmissao, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      dataMovimento: moment(dataEmissao, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      dataEntrega: moment(dataEmissao, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      usuario: usuario.username,
    });
    navigation.navigate('NovoItem', {
      idMov: id,
      dataEmissao: moment(dataEmissao, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      dataEntrega: moment(dataEmissao, 'DD/MM/YYYY').format('YYYY-MM-DD'),
    });
    Keyboard.dismiss();
  }

  return (
    <Container>
      <Header
        name="Novo"
        icon="arrow-circle-left"
        color="#fff"
        size={26}
        navigation={navigation.goBack}
      />
      <Form>
        <HeaderForm>
          <HeaderMov>
            <HeaderText>Identificador: </HeaderText>
            <HeaderText>
              <Bold>{id}</Bold>
            </HeaderText>
          </HeaderMov>
          <HeaderMov>
            <HeaderText>Aberto Financeiro: </HeaderText>

            <HeaderText>
              <Bold>
                R${' '}
                {String(saldo.toFixed(2))
                  .replace('.', ',')
                  .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') || 0}
              </Bold>
            </HeaderText>
          </HeaderMov>
        </HeaderForm>
        <Busca>
          <InputMask
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Buscar cliente"
            returnKeyType="send"
            mask="[00].[0000]"
            onChangeText={(text) => setCliente(text)}
            value={cliente}
            keyboardType="phone-pad"
          />

          <SearchButton onPress={() => handleSearchCliente()}>
            <FontAwesome name="search" size={20} color="#FFF" />
          </SearchButton>
        </Busca>

        <Input editable={false}>
          <Text>{nome}</Text>
        </Input>
        <Input editable={false}>
          <Text>{cnpj}</Text>
        </Input>

        <Input
          keyboardType="phone-pad"
          onChangeText={(data) => setDataEmissao(data)}
        >
          <Text>{dataEmissao}</Text>
        </Input>
        <SubmitButton
          onPress={() => handleAddItem()}
          disabled={!nome}
          backgroundColor={!nome ? 'rgba(204,0,0,0.4)' : 'rgba(204,0,0,1)'}
        >
          <TextButton>Adicionar Itens</TextButton>
        </SubmitButton>

        {/* <ActionContainer>
          <AddAction onPress={handleSaveMovimento}>
            <FontAwesome name="save" color="#fff" size={25} />
          </AddAction>
        </ActionContainer> */}
      </Form>
    </Container>
  );
}
