/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from 'react';
import {
  Keyboard,
  Platform,
  Alert,
  StyleSheet,
  Modal,
  View,
  Text,
  Linking,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { SwipeListView } from 'react-native-swipe-list-view';
import { useSelector } from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';
import { format, addDays } from 'date-fns';
import ActionButton from 'react-native-circular-action-menu';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Header from '~/components/Header';
import { api } from '../../services/api';
import Loading from '../../components/Loading';
import Error from '../../components/Error';
import {
  Container,
  InputContainer,
  Input,
  SubmitButton,
  PedidoEdit,
  PedidoButton,
  Name,
  PedidoDelete,
  PickerContainer,
  ButtonContainerModalRomaneio,
  ButtonModal,
  ButtonModalPeso,
  ContainerLista,
  HeaderContainer,
  PickerContainerTipoMovimento,
  ModalPeso,
  TextContainer,
  TextPeso,
  TextPesoHeader,
} from './styles';
import { ButtonContainer } from './Detalhes/styles';

const Movimentos = ({ navigation }) => {
  const usuario = useSelector((state) => state.usuario.profile);
  const [cliente, setCliente] = useState('');
  const [data, setData] = useState(addDays(new Date(), 1));
  const [pedidos, setPedidos] = useState([]);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [selectedMovimentos, setSelectedMovimentos] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [modalPesoVisible, setModalPesoVisible] = useState(false);
  const [romaneio, setRomaneio] = useState([]);
  const [tipoMovimento, setTipoMovimento] = useState('2.1.10');
  const [listaRomaneio, setListaRomaneios] = useState([]);
  const [pesos, setPesos] = useState([]);

  const dataFormat = format(data, 'yyyy-MM-dd', {
    timeZone: 'America/Sao_Paulo',
  });

  async function handleFindPedidoPorUsuario() {
    const response = await api.get(
      `movimentos?codigo=${cliente}&data=${dataFormat}&usuario=${
        usuario.username
      }&tipoMovimento=${tipoMovimento || '2.1.10'}`
    );
    setPedidos(response.data);
    Keyboard.dismiss();
    setCliente('');
  }

  const handleEditMovimento = (id) => {
    setIsLoading(true);
    navigation.navigate('Detalhes', { id });
    setIsLoading(false);
    setTipoMovimento('2.1.10');
  };

  useEffect(() => {
    navigation.addListener('focus', async () => {
      setError(null);
      setIsLoading(true);
      try {
        setTipoMovimento('2.1.10');
        await handleFindPedidoPorUsuario();
        setIsLoading(false);
        getRomaneios();
      } catch (err) {
        setTimeout(() => {
          setIsLoading(false);
          setError(err);
        }, 8000);
      }
    });
  }, [navigation]);

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  const handleNewPedido = async () => {
    const response = await api.get(`movimentos/novoPedido`);
    const { id, numeroMov } = response.data;
    return navigation.navigate('NovoMovimento', { id, numeroMov });
  };

  const handleDeleteMov = (id) => {
    try {
      api.delete(`movimento/${id}/delete`);
      const listPedido = pedidos.filter((item) => item.id !== id);
      setPedidos(listPedido);
    } catch (err) {
      Alert.alert(err);
    }
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || data;
    setShow(Platform.OS === 'ios');
    setData(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('data');
  };
  const getRomaneios = async () => {
    const fetchData = await api.get('romaneios');
    const romaneioData = fetchData.data;
    setListaRomaneios(romaneioData);
  };

  async function handleSelectMovimento(id) {
    const alreadySelected = selectedMovimentos.findIndex((item) => item === id);
    if (alreadySelected >= 0) {
      const filteredItems = selectedMovimentos.filter((item) => item !== id);
      setSelectedMovimentos(filteredItems);
    } else {
      setSelectedMovimentos([...selectedMovimentos, id]);
    }
  }

  const handleAddRomaneioPedido = async () => {
    if (selectedMovimentos.length === 0) {
      Alert.alert('Message', 'Selecione os pedidos');
      return;
    }
    if (romaneio === 0) {
      Alert.alert('Message', 'Selecione pelo menos 1 caminhão');
      return;
    }
    await api.put('movimentos/romaneios', { selectedMovimentos, romaneio });
    handleFindPedidoPorUsuario();
  };

  const handlePesoRomaneios = async () => {
    const response = await api.get(
      `movimentos/pesos?usuario=${usuario.username}&data=${dataFormat}`
    );
    setPesos(response.data);
  };

  const handleGeneratedReport = async () => {
    await api.get(
      `movimentos/generate-report?dataEmissao=${dataFormat}&usuario=${usuario.username.toLowerCase()}`
    );

    const linkReport = `https://server.angelelli.com.br/reports/${usuario.username.toLowerCase()}_pedidos.pdf`;

    await Linking.openURL(linkReport);
  };

  return (
    <Container>
      <Header
        name="Movimentos"
        icon="home"
        color="#fff"
        size={24}
        navigation={() => navigation.navigate('Home')}
        title="Pedidos"
      >
        {pedidos.length}
      </Header>
      <HeaderContainer>
        <InputContainer>
          <Input
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Buscar por cliente"
            returnKeyType="send"
            mask={'[00].[0000]'}
            onChangeText={(text) => setCliente(text)}
            value={cliente}
            keyboardType="phone-pad"
          />

          <SubmitButton
            value="Teste"
            onPress={showDatepicker}
            title="Show date picker!"
          >
            <MaterialIcons name="today" size={20} color="#c6222e" />
          </SubmitButton>
          <SubmitButton onPress={() => handleFindPedidoPorUsuario()}>
            <MaterialIcons name="search" size={20} color="#c6222e" />
          </SubmitButton>

          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              timeZoneOffsetInMinutes={0}
              value={data}
              mode={mode}
              is24Hour
              display="default"
              onChange={onChange}
            />
          )}
        </InputContainer>
        <PickerContainerTipoMovimento>
          <RNPickerSelect
            useNativeAndroidPickerStyle
            style={{ fontSize: 14, border: 1 }}
            onValueChange={(value) => setTipoMovimento(value)}
            placeholder={{
              label: 'Selecione o tipo de movimento',
              value: '2.1.10',
            }}
            items={[
              { label: 'A Faturar (2.1.10)', value: '2.1.10' },
              { label: 'Faturado (2.2.05)', value: '2.2.05' },
            ]}
          />
        </PickerContainerTipoMovimento>
      </HeaderContainer>

      <SwipeListView
        key={(pedido) => pedido.idmov}
        data={pedidos}
        distanceBetweenItem={12}
        keyExtractor={(pedido) => String(pedido.idmov)}
        renderItem={({ item }) => (
          <PedidoButton
            key={String(item.id)}
            romaneio={item.romaneio}
            // onPress={() => handleEditMovimento(item.id)}
            onPress={() => handleSelectMovimento(item.id)}
            activeOpacity={0.95}
            style={
              selectedMovimentos.includes(item.id) ? styles.selectedItem : {}
            }
          >
            <ContainerLista>
              <Name romaneio={item.romaneio}>{item.codcfo}</Name>
              <Name romaneio={item.romaneio}>
                {item.cliente.nome.substring(0, 25)}
              </Name>
            </ContainerLista>
            <ContainerLista>
              <Name romaneio={item.romaneio}>
                Romaneio:
                {item.cliente.campoLivre
                  ? item.cliente.campoLivre.substring(0, 2)
                  : ''}
              </Name>
              <Name romaneio={item.romaneio}>
                {item.romaneio || 'Não Romaneado'}
              </Name>
            </ContainerLista>
            <ContainerLista>
              <Name romaneio={item.romaneio}>Movimento N:</Name>
              <Name romaneio={item.romaneio}>{item.numeroMovimento}</Name>
            </ContainerLista>
          </PedidoButton>
        )}
        renderHiddenItem={({ item }) => (
          <>
            <PedidoEdit onPress={() => handleEditMovimento(item.id)}>
              <FontAwesome name="edit" color="#fff" size={26} />
            </PedidoEdit>
            {tipoMovimento === '2.1.10' && (
              <PedidoDelete onPress={() => handleDeleteMov(item.id)}>
                <FontAwesome name="trash" color="#fff" size={26} />
              </PedidoDelete>
            )}
          </>
        )}
        rightOpenValue={-85}
        leftOpenValue={85}
      />
      {tipoMovimento === '2.1.10' && (
        <>
          <ActionButton buttonColor="#399d4b" size={80}>
            <ActionButton.Item
              buttonColor="#399d4b"
              title="Novo pedido"
              size={60}
              onPress={handleNewPedido}
            >
              <FontAwesome5 name="plus" color="#fff" size={20} />
            </ActionButton.Item>

            <ActionButton.Item
              buttonColor="#0087cc"
              size={60}
              title="Romaneio"
              onPress={() => setModalVisible(true)}
            >
              <FontAwesome5 name="truck-moving" color="#fff" size={24} />
            </ActionButton.Item>

            <ActionButton.Item
              buttonColor="#9b59b6"
              title="Peso da carga"
              size={60}
              onPress={() => [handlePesoRomaneios(), setModalPesoVisible(true)]}
            >
              <FontAwesome5 name="balance-scale" color="#fff" size={20} />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#cb4459"
              size={60}
              title="Relatório"
              onPress={handleGeneratedReport}
            >
              <Ionicons name="md-document-text-sharp" color="#fff" size={24} />
            </ActionButton.Item>
          </ActionButton>
          {/* <ButtonReport onPress={handleGeneratedReport}>
            <Ionicons name="md-document-text-sharp" color="#fff" size={24} />
          </ButtonReport>
          <AddActionTruck onPress={() => setModalVisible(true)}>
            <FontAwesome5 name="truck-moving" color="#fff" size={20} />
          </AddActionTruck>
          <AddAction onPress={() => handleNewPedido()}>
            <FontAwesome5 name="plus" color="#fff" size={20} />
          </AddAction>
          <ButtonBalance
            onPress={() => [handlePesoRomaneios(), setModalPesoVisible(true)]}
          >
            <FontAwesome5 name="balance-scale" color="#fff" size={20} />
          </ButtonBalance> */}
        </>
      )}
      <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <PickerContainer>
              <RNPickerSelect
                useNativeAndroidPickerStyle
                style={{ fontSize: 14, border: 1 }}
                onValueChange={(value) => setRomaneio(value)}
                placeholder={{
                  label: 'Selecione o caminhão',
                }}
                items={listaRomaneio?.map((rom) => ({
                  label: rom.descricao,
                  value: rom.id,
                }))}
              />
            </PickerContainer>
            <ButtonContainerModalRomaneio>
              <ButtonModal
                onPress={() => [
                  handleAddRomaneioPedido(),
                  setSelectedMovimentos([]),
                  setModalVisible(!modalVisible),
                ]}
                style={{ backgroundColor: '#399d4b' }}
              >
                <Text style={styles.textStyle}>Salvar</Text>
              </ButtonModal>
              <ButtonModal
                onPress={() => setModalVisible(!modalVisible)}
                style={{ backgroundColor: '#c6222e' }}
              >
                <Text style={styles.textStyle}>Cancelar</Text>
              </ButtonModal>
            </ButtonContainerModalRomaneio>
          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent
        visible={modalPesoVisible}
        onRequestClose={() => {
          setModalPesoVisible(!modalPesoVisible);
        }}
      >
        <ModalPeso>
          <View style={styles.modalPesoView}>
            <TextPesoHeader>Peso dos romaneios</TextPesoHeader>
            {pesos.map((item) => (
              <TextContainer>
                <TextPeso>Caminhão: {item.romaneio}</TextPeso>
                <TextPeso>Peso Aproximado: {item.pesoCaminhao} KG</TextPeso>
              </TextContainer>
            ))}
            <ButtonContainer>
              <ButtonModalPeso
                onPress={() => setModalPesoVisible(!modalPesoVisible)}
              >
                <Text style={styles.textStyle}>Fechar</Text>
              </ButtonModalPeso>
            </ButtonContainer>
          </View>
        </ModalPeso>
      </Modal>
    </Container>
  );
};

const styles = StyleSheet.create({
  selectedItem: {
    borderColor: '#c6222e',
    borderWidth: 1.3,
  },
  deselectedItem: {
    borderColor: 0,
    borderWidth: 0,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    width: 280,
    height: 200,
    // margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    // paddingTop: 15,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalPesoView: {
    width: 280,
    height: 320,
    // margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    paddingLeft: 10,
    paddingTop: 15,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },

  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  actionButtonIcon: {
    fontSize: 20,
    height: 25,
    color: 'white',
  },
});

export default Movimentos;
