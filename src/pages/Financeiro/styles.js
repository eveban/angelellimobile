import styled from 'styled-components/native';
import { RectButton, FlatList } from 'react-native-gesture-handler';
import TextInputMask from 'react-native-text-input-mask';

export const Container = styled.View`
  flex: 1;
`;

export const InputContainer = styled.View`
  padding: 0 15px;
  flex-direction: row;
  justify-content: space-between;
  background: #c6222e;
  height: 70px;
`;

export const Input = styled(TextInputMask)`
  width: 80%;
  height: 50px;
  background: #fff;
  border-radius: 8px;
  padding: 0 15px;
`;

export const SubmitButton = styled(RectButton).attrs({
  placeholderTextColor: '#999',
})`
  justify-content: center;
  align-items: center;
  background: #fff;
  border-radius: 8px;
  margin-left: 10px;
  width: 50px;
  height: 50px;
`;

export const List = styled(FlatList).attrs({
  showsVerticalScrollIndicator: false,
})`
  margin-top: 10px;
`;

export const DataList = styled.TouchableOpacity`
  border-radius: 8px;
  margin: 5px 10px;
  background-color: #fff;
  elevation: 3;
  padding: 10px;
`;

export const DataContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const Name = styled.Text`
  flex: 1;
  text-align: center;
  font-size: 16px;
  color: #c6222e;
  margin-bottom: 1px;
  font-family: 'Ubuntu-Bold';
`;
export const Data = styled.Text`
  font-size: 16px;
  color: #c6222e;
  margin-bottom: 1px;
  font-family: 'Ubuntu-Bold';
`;
export const ValorOriginal = styled.Text`
  font-size: 16px;
  color: #0000cc;
  margin-bottom: 1px;
  font-family: 'Ubuntu-Bold';
`;
export const LiquidoData = styled.Text`
  font-size: 16px;
  color: #399d4b;
  margin-bottom: 1px;
  font-family: 'Ubuntu-Bold';
`;
