import React, { useState, useEffect } from 'react';
import { Linking } from 'react-native';
import { useSelector } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';

import Header from '../../components/Header';
import { api } from '../../services/api';

import Loading from '../../components/Loading';
import Error from '../../components/Error';

import {
  Container,
  InputContainer,
  Input,
  SubmitButton,
  List,
  DataList,
  DataContainer,
  Data,
  ValorOriginal,
  LiquidoData,
  Name,
} from './styles';

const Tabela = ({ navigation }) => {
  const [cliente, setCliente] = useState('');
  const [listAtrasados, setListAtrasados] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const usuario = useSelector((state) => state.usuario.profile);

  const saudacao = () => {
    const hour = new Date().getHours();
    if (hour < 12) {
      return 'Bom dia!';
    }
    return 'Boa tarde!';
  };

  const handleListAtrasados = async () => {
    setIsLoading(true);
    const { data } = await api.get(
      `/financeiro/titulos?usuario=${usuario.username}&cliente=${cliente}`
    );
    setListAtrasados(data);
    setIsLoading(false);
  };

  useEffect(() => {
    navigation.addListener('focus', () => {
      setIsLoading(true);
      setError(null);
      try {
        handleListAtrasados();
        setIsLoading(false);
      } catch (err) {
        setTimeout(() => {
          setIsLoading(false);
          setError(err);
        }, 8000);
      }
    });
  }, [navigation]);

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  function handleComposeWhatsApp(
    whats,
    numeroDocumento,
    dataVencimento,
    valor
  ) {
    const telefoneFormatado = whats?.replace(/\D+/g, '');
    Linking.openURL(
      `whatsapp://send?phone=55${telefoneFormatado}&text=${saudacao()}%0A` +
        `%0AGostaríamos de comunicar que seu título: ` +
        `%0ANúmero: ${numeroDocumento}%0AValor: R$ ${
          String(valor.toFixed(2))
            .replace('.', ',')
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') || 0
        }%0AVenceu em: ${moment(dataVencimento, 'YYYY-MM-DD').format(
          'DD/MM/YYYY'
        )}` +
        `%0AQual a programação de pagamento?`
    );
  }

  return (
    <Container>
      <Header
        name="Lista atrasados"
        icon="home"
        color="#fff"
        size={24}
        navigation={() => {
          navigation.navigate('Home');
        }}
      />
      <InputContainer>
        <Input
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Buscar por cliente"
          returnKeyType="send"
          mask="[00].[0000]"
          onChangeText={(text) => setCliente(text)}
          value={cliente}
          keyboardType="phone-pad"
        />

        <SubmitButton
          onPress={() => {
            handleListAtrasados();
          }}
        >
          <MaterialIcons name="search" size={20} color="#c6222e" />
        </SubmitButton>
      </InputContainer>
      {isLoading ? (
        <Loading />
      ) : (
        <List
          // key={({ item }) => item}
          data={listAtrasados}
          // distanceBetweenItem={12}
          keyExtractor={(item) => item.IDLAN}
          renderItem={({ item }) => (
            <DataList
              onPress={() =>
                handleComposeWhatsApp(
                  item.TELEX,
                  item.NUMERODOCUMENTO,
                  item.DATAVENCIMENTO,
                  item.VALORORIGINAL
                )
              }
            >
              <DataContainer>
                <Name>
                  {item.CODCFO} - {item.NOMEFANTASIA.substring(0, 23)}
                </Name>
              </DataContainer>
              <DataContainer>
                <Data>Documento</Data>
                <Data>{item.NUMERODOCUMENTO}</Data>
              </DataContainer>
              <DataContainer>
                <Data>Emissão:</Data>
                <Data>
                  {moment(item.DATAEMISSAO, 'YYYY-MM-DD').format('DD/MM/YYYY')}
                </Data>
              </DataContainer>
              <DataContainer>
                <Data>
                  Vencimento:{' '}
                  {moment(item.DATAVENCIMENTO, 'YYYY-MM-DD').format(
                    'DD/MM/YYYY'
                  )}
                </Data>
                <Data>Dias: {item.DIAS}</Data>
              </DataContainer>
              <DataContainer>
                <ValorOriginal>
                  V: R${' '}
                  {String(item.VALORORIGINAL.toFixed(2))
                    .replace('.', ',')
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') || 0}
                </ValorOriginal>
                <Data>
                  A: R${' '}
                  {String(item.ACRESCIMO.toFixed(2))
                    .replace('.', ',')
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') || 0}
                </Data>
                <LiquidoData>
                  L: R${' '}
                  {String(item.LIQUIDO.toFixed(2))
                    .replace('.', ',')
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') || 0}
                </LiquidoData>
              </DataContainer>
            </DataList>
          )}
        />
      )}
    </Container>
  );
};

export default Tabela;
