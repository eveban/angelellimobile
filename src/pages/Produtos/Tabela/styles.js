import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { TextInput, Platform } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

export const Container = styled.SafeAreaView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
})`
  flex: 1;
  background: #eee;
`;

export const HeaderContainer = styled.View`
  background-color: #c6222e;
`;

export const HeaderForm = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 15px;
  padding-bottom: 15px;
`;

export const ToggleButton = styled.TouchableOpacity`
  margin-top: 20px;
  margin-left: 30px;
`;
export const ContainerTributacao = styled.View`
  margin-top: 15px;
  flex-direction: row;
  align-items: center;
`;

export const PickerContainer = styled.View`
  flex: 1;
  height: 50px;
  background-color: #eee;
  border-radius: 5px;
  font-size: 16px;
  justify-content: center;
  padding: 15px;
  margin: 0 15px;
`;

export const Input = styled(TextInput)`
  flex: 1;
  height: 50px;
  background: #eee;
  border-radius: 4px;
  padding: 0 15px;
  margin-right: 15px;
`;

export const SubmitButton = styled(RectButton)`
  width: 50px;
  height: 50px;
  align-items: center;
  justify-content: center;
  color: #c6222e;
  background: #eee;
  border-radius: 8px;
`;

export const Pedido = styled.View`
  padding: 5px;
`;
export const Info = styled.View`
  border: 1px;
  border-color: #c6222e;
  height: 80px;
  background: #eee;
  border-radius: 8px;
  align-items: center;
  margin: 0 2px 5px;
  flex-direction: row;
  justify-content: space-between;
  elevation: 10;
`;

export const TitleProduto = styled.View`
  flex: 1;
  margin-left: 15px;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  background: #eee;
`;

export const NameTitle = styled.Text`
  font-size: 14px;
  color: #c6222e;
  font-weight: bold;
  margin-top: 4px;
`;
export const Preco = styled.View`
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: #c6222e;
  height: 78px;
  width: 100px;
  color: #eee;
  border-radius: 8px;
  border: 2px solid #eee;
`;
export const NamePreco = styled(TextInputMask)`
  font-size: 12px;
  color: #eee;
  font-weight: bold;
  margin-top: 4px;
  text-align: center;
  font-size: 18px;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})``;

export const Quantidade = styled.Text`
  font-size: 14px;
  color: #fff;
  font-weight: bold;
  margin-top: 4px;
  text-align: center;
`;

export const Codigo = styled.Text`
  font-size: 14px;
  color: #c6222e;
  font-weight: bold;
  margin-top: 4px;
  text-align: left;
`;
