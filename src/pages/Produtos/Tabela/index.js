import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { format, addDays } from 'date-fns';

import { signOut } from '~/store/modules/auth/actions';
import Header from '~/components/Header';
import { api } from '../../../services/api';
// import ToggleButton from '../../../components/ToggleButton';

import {
  Container,
  HeaderContainer,
  ToggleButton,
  HeaderForm,
  Input,
  SubmitButton,
  List,
  Pedido,
  Info,
  TitleProduto,
  Preco,
  NamePreco,
  NameTitle,
} from './styles';

const Tabela = ({ navigation }) => {
  const [produto, setProduto] = useState('');
  const [items, setItems] = useState([]);
  const dispatch = useDispatch();

  const dataFormat = format(addDays(new Date(), 1), 'yyyy-MM-dd', {
    timeZone: 'America/Sao_Paulo',
  });

  async function handlePreco() {
    try {
      if (produto.length >= 3 || produto.length === 0) {
        const response = await api.get(
          `tabela?dataVigencia=${dataFormat}&descricao=${produto}`
        );
        setItems(response.data);
      }
    } catch (error) {
      const erro = String(error);
      if (erro.includes('401')) {
        dispatch(signOut());
      }
    }

    // Keyboard.dismiss();
    // setProduto('');
  }

  useEffect(() => {
    handlePreco();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [produto]);

  return (
    <Container>
      <Header
        icon="home"
        size={24}
        color="#fff"
        navigation={() => navigation.navigate('Home')}
        name="Tabela de Preço"
      >
        <ToggleButton onPress={navigation.openDrawer}>
          <Icon name="home" size={24} color="#eee" />
        </ToggleButton>
      </Header>
      <HeaderContainer>
        <HeaderForm>
          <Input
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Buscar por produto"
            returnKeyType="send"
            onChangeText={(text) => setProduto(text)}
            value={produto}
            keyboardType="default"
          />
          <SubmitButton
            onPress={() => {
              handlePreco();
            }}
          >
            <Icon name="search" size={20} color="#c6222e" />
          </SubmitButton>
        </HeaderForm>
      </HeaderContainer>

      <List
        key={(item) => item.codigoAuxiliar}
        data={items}
        keyExtractor={(item) => item.codigoAuxiliar}
        renderItem={({ item }) => (
          <Pedido>
            <Info>
              <TitleProduto>
                <NameTitle>
                  {item.codigoAuxiliar} - {item.fantasia}
                </NameTitle>
                <NameTitle>{item.descAuxiliar}</NameTitle>
              </TitleProduto>
              <Preco>
                <NamePreco
                  type="money"
                  value={item.items.preco}
                  options={{
                    precision: 2,
                    separator: ',',
                    unit: 'R$ ',
                  }}
                />
                {/* <NamePreco>
                  {`${item.items.preco}` <= 0
                    ? 'Sob consulta'
                    : `R$ ${item.items.preco}`}
                </NamePreco> */}
              </Preco>
            </Info>
          </Pedido>
        )}
      />
    </Container>
  );
};

export default Tabela;
