import React, { useState } from 'react';
import { Keyboard, Platform } from 'react-native';
import { format } from 'date-fns';
import DateTimePicker from '@react-native-community/datetimepicker';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { api } from '../../services/api';
import {
  Container,
  Form,
  Input,
  SubmitButton,
  List,
  Pedido,
  Codigo,
  Name,
  ProfileButton,
  ProfileButtonText,
  Info,
  ActionContainer,
  AddAction,
} from './styles';

// eslint-disable-next-line no-console
// console.disableYellowBox = true;

export default function Movimentos({ navigation }) {
  const [cliente, setCliente] = useState('');
  const [data, setData] = useState(new Date());
  const [pedidos, setPedidos] = useState([]);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const dataFormat = format(data, 'yyyy-MM-dd', {
    timeZone: 'America/Sao_Paulo',
  });

  async function handleSearchPedidoPorCliente() {
    const response = await api.get(
      `movimentos?codigo=${cliente}&data=${dataFormat}`
    );

    setPedidos(response.data);
    Keyboard.dismiss();
    setCliente('');
  }

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || data;
    setShow(Platform.OS === 'ios');
    setData(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('data');
  };

  return (
    <Container>
      <Form>
        <Input
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Buscar por cliente"
          returnKeyType="send"
          mask="[00].[0000]"
          onChangeText={(text) => setCliente(text)}
          value={cliente}
          keyboardType="phone-pad"
        />

        <SubmitButton
          value="Teste"
          onPress={showDatepicker}
          title="Show date picker!"
        >
          <MaterialIcons name="today" size={20} color="#fff" />
        </SubmitButton>
        <SubmitButton onPress={handleSearchPedidoPorCliente()}>
          <MaterialIcons name="search" size={20} color="#fff" />
        </SubmitButton>

        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            value={data}
            mode={mode}
            is24Hour
            display="default"
            onChange={onChange}
          />
        )}
      </Form>

      <List
        key={(pedido) => pedido.idmov}
        data={pedidos}
        keyExtractor={(pedido) => pedido.idmov}
        renderItem={({ item }) => (
          <Pedido>
            <Info>
              <Codigo>Código: {item.idmov}</Codigo>
              <Name>
                {item.codcfo} - {item.cliente.nome}
              </Name>
              <Name>CNPJ: {item.cliente.cgccfo}</Name>
              <Name>
                Data:
                {item.dataMovimento}
              </Name>
            </Info>
            <ProfileButton onPress={() => {}}>
              <ProfileButtonText>Ver Itens</ProfileButtonText>
            </ProfileButton>
          </Pedido>
        )}
      />
      <ActionContainer>
        <AddAction onPress={() => navigation.navigate('NovoMovimento')}>
          <FontAwesome name="plus" color="#fff" size={25} />
        </AddAction>
      </ActionContainer>
    </Container>
  );
}
