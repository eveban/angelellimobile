import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import TextInputMask from 'react-native-text-input-mask';

export const Container = styled.View`
  flex: 1;
  background: #fff;
  /* padding-top: 40px; */
  padding: 30px 20px 5px;
`;
export const Form = styled.View`
  flex-direction: row;
  padding-bottom: 20px;
  /* border-bottom-width: 1px; */
  /* border-color: #056571; */
  margin: 0 5px 0;
`;
export const Input = styled(TextInputMask)`
  flex: 1;
  height: 40px;
  background: #eee;
  border-radius: 4px;
  padding: 0 15px;
  border: 1px solid #eee;
`;
export const SubmitButton = styled(RectButton).attrs({
  placeholderTextColor: '#999',
})`
  justify-content: center;
  align-items: center;
  background: #c6222e;
  border-radius: 4px;
  margin-left: 10px;
  width: 40px;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})`
  /* margin-top: 20px; */
`;

export const Pedido = styled.View`
  align-items: center;
  margin: 0 5px 30px;
`;
export const Info = styled.View`
  border: 1px;
  border-color: #c6222e;
  align-self: stretch;
  border-radius: 4px;
  align-items: center;
  margin: 0 2px 5px;
  padding: 0 5px;
`;

export const Name = styled.Text`
  font-size: 14px;
  color: #333;
  font-weight: bold;
  margin-top: 4px;
  text-align: center;
`;

export const Codigo = styled.Text`
  font-size: 14px;
  color: #333;
  font-weight: bold;
  margin-top: 4px;
  text-align: left;
`;

export const ProfileButton = styled(RectButton)`
  margin-top: 5px;
  align-self: stretch;
  border-radius: 4px;
  background: #c6222e;
  justify-content: center;
  align-items: center;
  height: 36px;
`;

export const ProfileButtonText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: #fff;
  text-transform: uppercase;
`;
export const ActionContainer = styled.View`
  flex: 1;
  justify-content: flex-end;
  align-items: flex-end;
`;

export const AddAction = styled.TouchableOpacity`
  /* border: 1px solid rgba(255, 255, 255, 0.9); */
  width: 70px;
  height: 70px;
  border-radius: 35px;
  background-color: #c6222e;
  justify-content: center;
  align-items: center;
  elevation: 7;
`;
