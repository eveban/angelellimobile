import { Image } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';

import Input from '~/components/Input';

export const Container = styled.View`
  flex: 1;
  background: #f7f7f7;
  align-items: center;
`;

export const Header = styled.View`
  width: 100%;
  height: 25%;
  background: #c6222e;

  border-bottom-right-radius: 500px;
`;

export const Form = styled.ScrollView`
  flex: 1;
  width: 100%;
  padding: 0 30px;
`;

export const Footer = styled.View`
  width: 100%;
  height: 15%;
  padding: 0 30px;
  background: #f5f5f5;
  border-top-right-radius: 75px;
`;

export const InputText = styled(Input)`
  /* margin-bottom: 10px; */
`;

export const Button = styled(RectButton)`
  /* flex: 1; */
  height: 64px;
  background: #399d4b;
  border-radius: 16px;

  /* margin: 30px; */
  padding: 20px;

  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`;

export const Logo = styled(Image)`
  margin-top: -60%;
`;

export const Text = styled.Text`
  font-family: 'Poppins-Bold';
  font-size: 32px;
  letter-spacing: 1px;
  color: #564242;
`;

export const TextButton = styled.Text`
  font-family: 'Poppins-SemiBold';
  font-size: 16px;
  letter-spacing: 1px;
  color: #fff;
`;
