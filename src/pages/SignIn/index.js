import React, { useRef, useState } from 'react';
import { Alert, StatusBar, ActivityIndicator } from 'react-native';
import { useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';

import logo from '~/assets/meat3.png';

import { signInRequest } from '~/store/modules/auth/actions';

import {
  Container,
  Header,
  Form,
  InputText,
  Button,
  Logo,
  Text,
  Footer,
  TextButton,
} from './styles';

export default function SignIn() {
  const dispatch = useDispatch();
  const senhaRef = useRef();

  const [username, setUsername] = useState('');
  const [senha, setSenha] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  // const loading = useSelector((state) => state.auth.loading);

  function handleSubmit() {
    setIsLoading(true);
    try {
      dispatch(signInRequest(username.toLowerCase().trim(), senha));
    } catch (error) {
      Alert.alert(error);
    } finally {
      setIsLoading(false);
    }
  }

  return (
    <>
      <StatusBar
        barStyle="light-content"
        backgroundColor="#c6222e"
        networkActivityIndicatorVisible
      />
      <Container>
        <Header />
        <Logo
          source={logo}
          style={{
            flex: 1.8,
            resizeMode: 'contain',
            width: '90%',
            // height: 300,
          }}
        />

        <Form>
          <Text>Login</Text>
          <InputText
            icon="person-outline"
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Digite seu usuário"
            returnKeyType="next"
            onSubmitEditing={() => senhaRef.current.focus()}
            value={username}
            onChangeText={setUsername}
          />

          <InputText
            icon="lock-outline"
            secureTextEntry
            autoCapitalize="none"
            placeholder="Sua senha"
            ref={senhaRef}
            returnKeyType="send"
            onSubmitEditing={() => handleSubmit()}
            value={senha}
            onChangeText={setSenha}
          />
        </Form>

        <Footer>
          <Button loading={isLoading} onPress={() => handleSubmit()}>
            <Icon
              name="login"
              size={24}
              color="#FFF"
              style={{
                borderRightWidth: 2,
                width: 40,
                borderRightColor: 'rgba(255,255,255,0.4)',
              }}
            />
            {isLoading ? (
              <ActivityIndicator size="small" color="#FFF" />
            ) : (
              <TextButton>Entrar</TextButton>
            )}

            <Icon name="arrow-right" size={30} color="#399d4b" />
          </Button>
        </Footer>
      </Container>
    </>
  );
}
