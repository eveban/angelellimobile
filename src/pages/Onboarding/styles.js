import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
`;
export const Header = styled.View`
  height: 230px;
  align-items: center;
  padding: 30px 20px 0px 20px;

  background: #c6222e;
  border-bottom-left-radius: 1000px;
`;

export const ImageBackground = styled.Image`
  margin-top: 25px;
`;

export const ContainerText = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.Text`
  font-family: 'Poppins-Bold';
  font-size: 32px;
  margin-top: -15px;
  letter-spacing: 1px;
  color: #564242;
`;

export const TextCliente = styled.Text`
  padding: 0;
  margin-top: -30px;
  font-family: 'Damion-Regular';
  font-size: 48px;
  letter-spacing: 2px;
  color: #c6222e;
`;

export const TextDescription = styled.Text`
  font-family: 'Poppins-Regular';
  font-size: 16px;
  letter-spacing: 1px;
  margin: 0 25px;
  color: #564242;
`;
export const ContainerButton = styled(RectButton)`
  align-items: center;
  flex-direction: row;
`;
export const Button = styled(RectButton)`
  flex: 1;
  height: 60px;
  background: #399d4b;
  border-radius: 16px;

  margin: 30px;
  padding: 20px;

  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`;

export const TextButton = styled.Text`
  font-family: 'Poppins-SemiBold';
  font-size: 16px;
  letter-spacing: 1px;
  color: #fff;
`;
