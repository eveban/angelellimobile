import React from 'react';
import { StatusBar } from 'react-native';

import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import meatImage from '../../assets/meat2.png';

import {
  Container,
  Header,
  ImageBackground,
  ContainerText,
  Text,
  TextCliente,
  TextDescription,
  ContainerButton,
  Button,
  TextButton,
} from './styles';

const Onboarding = () => {
  const navigation = useNavigation();
  return (
    <>
      <StatusBar
        barStyle="light-content"
        backgroundColor="#c6222e"
        networkActivityIndicatorVisible
      />
      <Container>
        <Header>
          <ImageBackground
            source={meatImage}
            style={{
              resizeMode: 'contain',
              width: 500,
              height: 250,
            }}
          />
        </Header>
        <ContainerText>
          <Text>Com a</Text>
          <Text>qualidade</Text>
          <TextCliente>que seu cliente</TextCliente>
          <Text>merece!</Text>
          <TextDescription />
          <TextDescription />
          <TextDescription>A forma mais simples,</TextDescription>
          <TextDescription>de lançar seus pedidos.</TextDescription>
        </ContainerText>

        <ContainerButton>
          <Button onPress={() => navigation.navigate('SignIn')}>
            <Icon
              name="arrow-forward-ios"
              size={20}
              color="#FFF"
              style={{
                borderRightWidth: 2,
                width: 40,
                borderRightColor: 'rgba(255,255,255,0.4)',
              }}
            />
            <TextButton>Acessar</TextButton>
            <Icon name="arrow-forward-ios" size={30} color="#399d4b" />
          </Button>
        </ContainerButton>
      </Container>
    </>
  );
};

export default Onboarding;
