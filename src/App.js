import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';
import { signOut } from '~/store/modules/auth/actions';
import Routes from './routes';
import { api } from './services/api';

export default function App() {
  console.disableYellowBox = true;
  const signed = useSelector((state) => state.auth.signed);
  const dispatch = useDispatch();
  const UNAUTHORIZED = 401;

  api.interceptors.response.use(
    (response) => response,
    (error) => {
      const { status } = error.response;
      if (status && status === UNAUTHORIZED) {
        dispatch(signOut());
      }
      return Promise.reject(error);
    }
  );

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <>
      <StatusBar
        barStyle="light-content"
        backgroundColor="#c6222e"
        networkActivityIndicatorVisible
      />
      <NavigationContainer>
        <Routes signed={signed} />
      </NavigationContainer>
    </>
  );
}
