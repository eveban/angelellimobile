import './config/ReactotronConfig';
import React from 'react';
import { StatusBar, Platform, View } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';

// import Routes from './routes';
import { store, persistor } from './store';

import App from './App';

export default function Index() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <View
          style={{
            backgroundColor: '#c6222e',
            height: Platform.OS === 'ios' ? 20 : 0,
          }}
        >
          <StatusBar
            barStyle="light-content"
            backgroundColor="#c6222e"
            networkActivityIndicatorVisible
          />
        </View>
        <App />
      </PersistGate>
    </Provider>
  );
}
