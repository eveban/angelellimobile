import React from 'react';
import { useSelector } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Onboarding from './pages/Onboarding';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';

import Home from './pages/Home';
import Cliente from './pages/Clientes';
import Movimentos from './pages/Movimentos';
import NovoMovimento from './pages/Movimentos/Novo';
import NovoItem from './pages/Movimentos/Item';
import Tabela from './pages/Produtos/Tabela';
import Financeiro from './pages/Financeiro';
import Detalhe from './pages/Movimentos/Detalhes';

const Navigation = () => {
  AsyncStorage.clear();
  const signed = useSelector((state) => state.auth.signed);
  const HomeStack = createStackNavigator();
  const HomeStackScreen = () => (
    <HomeStack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerStyle: { backgroundColor: '#252525' },
        headerTintColor: '#FFF',
        headerTitleAlign: 'center',
        headerShow: 'none',
        animationEnabled: false,
      }}
    >
      <HomeStack.Screen
        name="Cliente"
        component={Cliente}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false, headerLeft: null }}
      />

      <HomeStack.Screen
        name="Movimentos"
        component={Movimentos}
        options={{ headerShown: false, headerLeft: null }}
      />

      <HomeStack.Screen
        name="NovoMovimento"
        component={NovoMovimento}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="NovoItem"
        component={NovoItem}
        options={{ headerShown: false, headerLeft: null }}
      />
      <HomeStack.Screen
        name="Detalhes"
        component={Detalhe}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="Tabela"
        component={Tabela}
        options={{ headerShown: false }}
      />
    </HomeStack.Navigator>
  );

  const Drawer = createDrawerNavigator();
  const DrawerScreen = () => (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={HomeStackScreen} />
      <Drawer.Screen name="Tabela de preços" component={Tabela} />
      <Drawer.Screen name="Financeiro" component={Financeiro} />
      <Drawer.Screen name="Movimentos" component={Movimentos} />
    </Drawer.Navigator>
  );

  const AuthStack = createStackNavigator();

  return (
    <AuthStack.Navigator
      screenOptions={{
        headerStyle: { backgroundColor: '#252525' },
        headerTintColor: '#FFF',
        headerShown: 'none',
        headerTitleAlign: 'center',
      }}
    >
      {signed ? (
        <AuthStack.Screen
          name="Home"
          component={DrawerScreen}
          options={{
            headerTitleAlign: 'center',
            headerShown: false,
          }}
        />
      ) : (
        <>
          <AuthStack.Screen
            name="Onboarding"
            component={Onboarding}
            options={{
              headerShown: false,
            }}
          />
          <AuthStack.Screen
            name="SignIn"
            component={SignIn}
            options={{
              headerShown: false,
            }}
          />
          <AuthStack.Screen name="SignUp" component={SignUp} />
        </>
      )}
    </AuthStack.Navigator>
  );
};
export default Navigation;
