<h1 align="center">
     🚀 <a href="#" alt="site do ecoleta"> Aplicativo Mobile (Pedido Digital) </a>
</h1>

<h3 align="center">
    Desenvolvimento Mobile com React Native CLI
</h3>
</br>
<h4 align="center">
 🚧   Concluído 🚀 🚧
</h4>

# Tabela de conteúdos

<!--ts-->

- [Tabela de conteúdos](#tabela-de-conteúdos)
  - [Sobre o projeto](#sobre-o-projeto)
  - [Funcionalidades](#funcionalidades)
  - [Como executar o projeto](#como-executar-o-projeto)
    - [Pré-requisitos](#pré-requisitos)
      - [Executando](#executando)
  - [Tecnologias](#tecnologias)
  - [Publicação](#publicação)
  - [Desenvolvido](#desenvolvido)

## Sobre o projeto

Projeto desenvolvido em ReactJS para consulta de dados de clientes inadimplentes.
<br />

## Funcionalidades

- [x] Listagem de clientes:

  - [x] Busca clientes ativos por código
  - [x] Lista clientes com títulos atrasados
  - [x] Envia mensagem do título em atraso pelo WhatsApp, com as informações do título

---

- [x] Movimento:

  - [x] Lançamento de pedidos, integrado ao backend com o Sistema TOTVS (RM)
  - [x] Busca produtos
  - [x] Verifica se o cliente está bloqueado, se há algum pendência financeira
  - [x] Tabela de Preços

<br />

## Como executar o projeto

### Pré-requisitos

- Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
  [Git](https://git-scm.com), [Node.js](https://nodejs.org/en/) e [Yarn](https://yarnpkg.com/).
  Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/).

- <p>Configurar ambiente de desenvolvimento do [React Native](https://nodejs.org/pt-br/blog/release/v16.20.0)</p>
- <p>A versão do Node que precisar para executar o projeto é a [16.20.0](https://nodejs.org/pt-br/blog/release/v16.20.0)</p>

#### Executando

</br>

```bash

# Clone este repositório
$ git clone https://eveban@bitbucket.org/eveban/angelellimobile.git .

# Acesse a pasta do projeto no terminal/cmd
$ cd angelellimobile

# Instale as dependências
$ npm install
# ou
$ yarn

# Execute a aplicação em modo de desenvolvimento
$ npm run android (ou ios)
# ou
$ yarn android (ou ios)

# O servidor inciará na porta:3333 - acesse http://localhost:3333

```

## Tecnologias

---

As seguintes ferramentas foram usadas na construção do projeto:

**Mobile** ([React Native CLI](https://reactnative.dev/))

- **[Styled Components](https://www.styled-components.com/)**
- **[Date-fns](https://date-fns.org/)**
- **[Redux](https://redux.js.org/)**
- **[Redux-Saga](https://redux-saga.js.org/)**

## Publicação

---

Projeto publicado na [Google Play](https://play.google.com/store/apps/details?id=com.angelmobile).

## Desenvolvido

---

Feito por Everson Silva 👋🏽 [Entre em contato!](https://www.linkedin.com/in/everson-silva-77bb1513/)
